﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DefaultBB : MonoBehaviour {
	public int damage = 1;
	public Transform shotPrefab;
	
	void Start () {}
	
	void Update () {}
	
	public void BoomRender (){
		var shotTransform = Instantiate(shotPrefab) as Transform;
		shotTransform.position = transform.position;
		Destroy(shotTransform.gameObject, 2);
	}
	
	void OnTriggerEnter2D(Collider2D otherCollider){
	  BoomScript shot = otherCollider.gameObject.GetComponent<BoomScript>();
		if (shot != null){
			
			Destroy(gameObject, 2);
		}
  }
}
