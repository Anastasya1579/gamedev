﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Threading;
using System;


public class MovePlayer : MonoBehaviour {

    public Vector2 speed = new Vector2(1, 1); //скорость
    private Vector2 movement; //направлние
    private Rigidbody2D pl;
    public int hp = 2;
	public int goldCount = 0;
	public int keeCount = 0;
	public Transform textPrefab;
    
    void Start () { 
		pl = this.GetComponent<Rigidbody2D>();
	}
	
	void Update () {
        float inputX = Input.GetAxis("Horizontal");
        float inputY = Input.GetAxis("Vertical");
        movement = new Vector2(speed.x * inputX, speed.y * inputY);
		WeaponScript weapon = GetComponent<WeaponScript>();
		if (Input.GetKey(KeyCode.Space)){
			weapon.Attack();
			Thread.Sleep(100);
        }
		
    }

    void FixedUpdate(){
        pl.velocity = movement;
    }
	
	public void Damage(int damageCount){
		hp -= damageCount;
		speed = new Vector2(1, 1);
		
		var textSprite = Instantiate(textPrefab) as Transform;
		textSprite.position = transform.position;
		ShowText showMe = textSprite.gameObject.GetComponent<ShowText>();		
		if (hp <= 0){
			gameObject.GetComponent<Renderer>().enabled = false;
			showMe.text = "You will never win! \nMuhahaha!";
		}else showMe.text = "Boom! \nYou have been damaged!";
	}
	
	void OnTriggerEnter2D(Collider2D otherCollider){
		
		/*
		*TODO: сделать отсчет времени в случае, если бомбу ставит персонаж
		*Взрыв персонажа:
		*Проверить, успел ли убежать (посчитать гипонузу, должна быть примерно меньше 1.2)
		double gip = (pl.position[0]-lshot.posX)*(pl.position[0]-lshot.posX)+(pl.position[1]-lshot.posY)*(pl.position[1]-lshot.posY)	;
		}*/
		
		DefaultBB shot = otherCollider.gameObject.GetComponent<DefaultBB>();
		if (shot != null){
			Damage(shot.damage);
			shot.BoomRender();
			Destroy(shot.gameObject);
		}
		
		GoldPick gold = otherCollider.gameObject.GetComponent<GoldPick>();
		if (gold != null){
			Destroy(gold.gameObject);
			goldCount++;
		}
		
		CollectKee kee = otherCollider.gameObject.GetComponent<CollectKee>();
		if (kee != null){
			var textSprite = Instantiate(textPrefab) as Transform;
			textSprite.position = transform.position;
			ShowText showMe = textSprite.gameObject.GetComponent<ShowText>();
			showMe.text = "Congratulations! \nThe door is unlocked!";			
			Destroy(kee.gameObject);
			keeCount++;
		}
		
		Unlock door = otherCollider.gameObject.GetComponent<Unlock>();
		if (door != null){ 
			var textSprite = Instantiate(textPrefab) as Transform;
			textSprite.position = transform.position;
			ShowText showMe = textSprite.gameObject.GetComponent<ShowText>(); 
			if(keeCount>0){
				Destroy(door.gameObject);
				keeCount++;
				showMe.text = "Wow! \nLevel comlite!";
			}else{
				showMe.text = "The door is locked. \n You should try to find a kee.";
			}
		}	
	}
	
	void OnGUI () { 
		GUI.Box (new Rect (25, 25, 250, 120), "Our Level: " + 1 + "\n\nHealth: " + hp + "\nGold Count: " + goldCount + "\nKee Collect: " + keeCount + "/1" + "\n\nPlayer position: " + pl.position); 
	} 
	
}
