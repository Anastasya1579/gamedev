﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Threading;

public class StoneCrash : MonoBehaviour {
	
	void OnTriggerEnter2D(Collider2D otherCollider){
		BoomScript shot = otherCollider.gameObject.GetComponent<BoomScript>();
		if (shot != null){
			Destroy(gameObject, 2);
		}
	}
}
