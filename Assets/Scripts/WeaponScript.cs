﻿using UnityEngine;
using System.Threading;
using System;

public class WeaponScript : MonoBehaviour
{
  public Transform shotPrefab;

  public void Attack(){
	var shotTransform = Instantiate(shotPrefab) as Transform;
	shotTransform.position = transform.position;
	BoomScript shot = shotTransform.gameObject.GetComponent<BoomScript>(); 
  }
}