﻿using UnityEngine;
using System.Threading;
using System.Collections;
using System;

public class BoomScript : MonoBehaviour
{
  public int damage = 1;
  private Rigidbody2D bm;
  public double posX;
  public double posY;
  public Transform shotPrefab;

  void Start(){
	  Destroy(gameObject, 2);
	  bm = this.GetComponent<Rigidbody2D>();
	  posX = bm.position[0];
	  posY = bm.position[1];
	  Invoke ("BoomRender", 1);
  }
  
  void BoomRender(){
		var shotTransform = Instantiate(shotPrefab) as Transform;
		shotTransform.position = transform.position;
		/* Увеличение взрыва в размерах. Как тут правильно спать???
		for(int i=1; i<=3;i++){
			shotTransform.transform.localScale -= new Vector3(Convert.ToSingle(i*0.5), Convert.ToSingle(i*0.5), 0.0f);
		}*/
		Destroy(shotTransform.gameObject, 1);
	}  
}